<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Registrasi</title>
</head>
<body>
    <h3>SILAHKAN ISI FORM REGISTRASI DIBAWAH INI :</h3>
    
        <form action="/daftar" method="POST">
            @csrf
            <label for="">Nama Depan :</label><br>
            <input type="text" name="nd" required><br><br>
            <label for="">Nama Belakang :</label><br>
            <input type="text" name="nb" required><br><br>
            <label for="email">Email :</label><br>
            <input type="email" ><br><br>
            <label for="jenis-kelamin">Jenis Kelamin</label><br>
            <input type="radio" name="jenis-kelamin" id="laki-laki">
            <label for="laki-laki">laki-Laki</label><br>
            <input type="radio" name="jenis-kelamin" id="perempuan">
            <label for="perempuan">Perempuan</label><br><br>
            <label for="hobby">Hobby :</label><br>
            <textarea name="hobby" id="hobby" cols="30" rows="10"></textarea><br><br>
            <label for="kewarganegaraan">Kewarganegaraan : </label><br><br>
            <select name="wni" id="wni">
                <option value="wni">Warga Negara Indonesia</option>
                <option value="wna">Warga Negara Asing</option>
            </select><br><br>
            <label for="bahasa-jago">Bahasa yang Anda kuasai :</label><br>
            <input type="checkbox" name="bahasa-indoneisa" id="bahasa-indoneisa">
            <label for="bahasa-indoneisa">Bahasa Indonesia</label><br>
            <input type="checkbox" name="bahasa-inggris" id="bahasa-inggris">
            <label for="bahasa-inggris">Bahasa Inggris</label><br>
            <input type="checkbox" name="bahasa-Spanyol" id="bahasa-Spanyol">
            <label for="bahasa-Spanyol">Bahasa Spanyol</label><br>
            <input type="checkbox" name="bahasa-Arab" id="bahasa-Arab">
            <label for="bahasa-Arab">Bahasa Arab</label><br><br><br>
            <button type="submit">Daftar</button>
        </form>
       
</body>
</html>