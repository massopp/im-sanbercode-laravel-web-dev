<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WelcomeController;
use App\Http\Controllers\RegisterController;
// use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [WelcomeController::class, "index"]);

Route::get('/formRegistrasi', [RegisterController::class, "daftarUser"]); //ini untuk menuju ke form registrasi

Route::post('/daftar', [RegisterController::class, "regis"]); //ini ketika klik tombol daftar 