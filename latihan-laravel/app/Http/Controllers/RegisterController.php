<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function daftarUser(){
        return view("pages.formRegistrasi");
    }
    public function regis(Request $request){
        $namaDepan = $request ['nd'];
        $namaBelakang = $request ['nb'];

        return view('pages.home', ["namaDepan" => $namaDepan, "namaBelakang" => $namaBelakang]);
    }
}
