<?php
require_once("animal.php");
require_once("frog.php");
require_once("ape.php");

echo "<h2>Latihan OOP</h2>";

$name1 = new Animals("Shaun");
echo "Name : " . $name1->namaAnimals. "<br>";
echo "Legs : " . $name1->legs . "<br>";
echo "Cold Blooded : " . $name1->coldBlooded . "<br>";
echo "<br>";

$name2 = new Animals("Buduk");
echo "Name : " . $name2->namaAnimals. "<br>";
echo "Legs : " . $name2->legs . "<br>";
echo "Cold Blooded : " . $name2->coldBlooded . "<br>";
echo "<br>";


$name4 = new Ape("Kera Sakti");
echo "Name : " . $name4->namaAnimals. "<br>";
echo "Legs : " . $name4->legs . "<br>";
echo "Cold Blooded : " . $name4->coldBlooded . "<br>";
echo "Yell : " . $name4->yell . "<br>";
?>
